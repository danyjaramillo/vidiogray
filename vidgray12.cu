#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

#define gpuErrchk(ans) { gpuAssert( (ans), __FILE__, __LINE__ ); }
inline void
gpuAssert( cudaError_t code, const char * file, int line, bool abort = true )
{
	if ( cudaSuccess != code )
	{
		fprintf( stderr, "\nGPUassert: %s %s %d\n", cudaGetErrorString( code ), file, line );
		if ( abort )
			exit( code );
	}
	return;
}

void convertFrameToGrayscaleCPU(
		uchar *datosFrameEntrada,
		uchar *datosFrameSalida,
		int width,
		int height);
__global__ void convertFrameToGrayscaleGPU(
		uchar *datosFrameEntrada,
		uchar *datosFrameSalida,
		int width,
		int height);

int main(int argc, char **argv)
{
	if(argc!=3)
	{
		printf("[ERR] Usage: %s Input.avi Output.avi\n",argv[0]);
		exit(0);
	}
	VideoCapture videoEntrada(argv[1]);
	if(!videoEntrada.isOpened())
	{
		printf("[ERR] Couldn't open file %s as input video\n",argv[1]);
		exit(0);
	}
	int width = videoEntrada.get(CV_CAP_PROP_FRAME_WIDTH);
	int height = videoEntrada.get(CV_CAP_PROP_FRAME_HEIGHT);
	Size videoSize(width,height);
	VideoWriter videoSalida(
			string(argv[2]),
			CV_FOURCC('M','P','4','V'),
			videoEntrada.get(CV_CAP_PROP_FPS),
			videoSize);
	if(!videoSalida.isOpened())
	{
		printf("[ERR] Couldn't open file %s as output video\n",argv[2]);
		videoEntrada.release();
		exit(0);
	}
	Mat frameCapturado, frameSalida;
	uchar *datosFrameSalida = (uchar *)malloc(
			sizeof(uchar)*width*height*3);
	uchar *d_datosFrameCapturado, *d_datosFrameSalida;
	cudaMalloc(
			(void **)&d_datosFrameCapturado,
			sizeof(uchar)*width*height*3);
	cudaMalloc(
			(void **)&d_datosFrameSalida,
			sizeof(uchar)*width*height*3);
	dim3 nBlock(32);
	dim3 nGrid(ceil((float)height*(float)width/(float)nBlock.x));
	while(videoEntrada.read(frameCapturado))
	{
		cudaMemcpy(
				d_datosFrameCapturado,
				frameCapturado.data,
				sizeof(uchar)*height*width*3,
				cudaMemcpyHostToDevice);
		frameCapturado.release();
		convertFrameToGrayscaleGPU<<<nGrid,nBlock>>>(
				d_datosFrameCapturado,
				d_datosFrameSalida,
				width,
				height
		);
		cudaDeviceSynchronize();
		/*convertFrameToGrayscaleCPU(
				frameCapturado.data,
				datosFrameSalida,
				width,
				height);*/
		cudaMemcpy(
				datosFrameSalida,
				d_datosFrameSalida,
				sizeof(uchar)*height*width*3,
				cudaMemcpyDeviceToHost);
		frameSalida =
				Mat(videoSize,CV_8UC3,datosFrameSalida);
		videoSalida.write(frameSalida);
	}
	cudaFree(d_datosFrameCapturado);
	cudaFree(d_datosFrameSalida);
	free(datosFrameSalida);
	videoSalida.release();
	videoEntrada.release();
	return 0;
}
void convertFrameToGrayscaleCPU(
		uchar *datosFrameEntrada,
		uchar *datosFrameSalida,
		int width,
		int height)
{
	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
		{
			size_t pixelIdx = 3*(i*width+j);
			uchar b = datosFrameEntrada[pixelIdx];
			uchar g = datosFrameEntrada[pixelIdx+1];
			uchar r = datosFrameEntrada[pixelIdx+2];
			uchar val = round(
					(float)r*0.21+
					(float)g*0.72+
					(float)b*0.07);

			datosFrameSalida[pixelIdx] = val;
			datosFrameSalida[pixelIdx+1] = val;
			datosFrameSalida[pixelIdx+2] = val;
		}
}
__global__ void convertFrameToGrayscaleGPU(
		uchar *datosFrameEntrada,
		uchar *datosFrameSalida,
		int width,
		int height)
{
	size_t pixelIdx = 3*(blockIdx.x*blockDim.x+threadIdx.x);
	if(pixelIdx>=width*height*3) return;
	uchar b = datosFrameEntrada[pixelIdx];
	uchar g = datosFrameEntrada[pixelIdx+1];
	uchar r = datosFrameEntrada[pixelIdx+2];
	uchar val = round(
			(float)r*0.21+
			(float)g*0.72+
			(float)b*0.07);

	datosFrameSalida[pixelIdx] = val;
	datosFrameSalida[pixelIdx+1] = val;
	datosFrameSalida[pixelIdx+2] = val;
}
